# dev_bd_light_server_v141

- flash software onto a dev kit (BRD4104A)
- provision device using dev_bd_provisioner_v14 on dev kit (BRD4104A)
- use segger tool JLinkRTTViewer.exe (using BGM13P32F512GA) to see display output 
use (EFR32BG13P)

- pressing PB0 while reseting board will do a factory reset
(push and hold PB0 then use reset then release PB0)

- PB0 publishes light sensor message light value = 0x00 0x00 0x00

- PB1 publishes ligh sensor message light value = 0x55 0xAA 0x54

- provisioner sets up the publish / subscription addresses

- self provisioning sets up publish / subscription addresses

# self provisioning
  - uncomment #define	SELF_PROVISION_ENABLED 1 and rebuild
  - flash
  - do factory reset
  
# provisioning using provisioning tool
  - comment #define	SELF_PROVISION_ENABLED and rebuild
  - flash 
  - do factory reset
  - use embedded provsioning tool
