//-----------------------------------------------------------------------------
//   All Rights Reserved
//  Intermatic Incorporated
//  Copyright 2019
//-----------------------------------------------------------------------------
//
//  Description: Functions here are common for more than one mesh model
//-----------------------------------------------------------------------------

#include <stdio.h>
#include "mesh_model_common.h"
//#include "debug.h"
#define DBGS 1

// Function implementation
uint8_t* get_api_error_text(errorcode_t error_code)
{
	static uint8_t buffer[50];
	memset(buffer, '\0', sizeof(buffer));

#if (!DBGS)	// if debug output is off only return the error code as a string
	sprintf((char *)buffer, "%x", error_code);
#else		// else take up a bunch of flash with the strings
	switch (error_code)
	{
		default:
			sprintf((char *)buffer, "Unknown Error Code %x", error_code);
			break;
		case bg_err_hardware_ps_store_full:
			sprintf((char *)buffer, "bg_err_hardware_ps_store_full");
			break;
		case bg_err_hardware_ps_key_not_found:
			sprintf((char *)buffer, "bg_err_hardware_ps_key_not_found");
			break;
		case bg_err_hardware_i2c_ack_missing:
			sprintf((char *)buffer, "bg_err_hardware_i2c_ack_missing");
			break;
		case bg_err_hardware_i2c_timeout:
			sprintf((char *)buffer, "bg_err_hardware_i2c_timeout");
			break;
		case bg_err_success:
			sprintf((char *)buffer, "bg_err_success");
			break;
		case bg_err_invalid_conn_handle:
			sprintf((char *)buffer, "bg_err_invalid_conn_handle");
			break;
		case bg_err_waiting_response:
			sprintf((char *)buffer, "bg_err_waiting_response");
			break;
		case bg_err_gatt_connection_timeout:
			sprintf((char *)buffer, "bg_err_gatt_connection_timeout");
			break;
		case bg_err_invalid_param:
			sprintf((char *)buffer, "bg_err_invalid_param");
			break;
		case bg_err_wrong_state:
			sprintf((char *)buffer, "bg_err_wrong_state");
			break;
		case bg_err_out_of_memory:
			sprintf((char *)buffer, "bg_err_out_of_memory");
			break;
		case bg_err_not_implemented:
			sprintf((char *)buffer, "bg_err_not_implemented");
			break;
		case bg_err_invalid_command:
			sprintf((char *)buffer, "bg_err_invalid_command");
			break;
		case bg_err_timeout:
			sprintf((char *)buffer, "bg_err_timeout");
			break;
		case bg_err_not_connected:
			sprintf((char *)buffer, "bg_err_not_connected");
			break;
		case bg_err_flow:
			sprintf((char *)buffer, "bg_err_flow");
			break;
		case bg_err_user_attribute:
			sprintf((char *)buffer, "bg_err_user_attribute");
			break;
		case bg_err_invalid_license_key:
			sprintf((char *)buffer, "bg_err_invalid_license_key");
			break;
		case bg_err_command_too_long:
			sprintf((char *)buffer, "bg_err_command_too_long");
			break;
		case bg_err_out_of_bonds:
			sprintf((char *)buffer, "bg_err_out_of_bonds");
			break;
		case bg_err_unspecified:
			sprintf((char *)buffer, "bg_err_unspecified");
			break;
		case bg_err_hardware:
			sprintf((char *)buffer, "bg_err_hardware");
			break;
		case bg_err_buffers_full:
			sprintf((char *)buffer, "bg_err_buffers_full");
			break;
		case bg_err_disconnected:
			sprintf((char *)buffer, "bg_err_disconnected");
			break;
		case bg_err_too_many_requests:
			sprintf((char *)buffer, "bg_err_too_many_requests");
			break;
		case bg_err_not_supported:
			sprintf((char *)buffer, "bg_err_not_supported");
			break;
		case bg_err_no_bonding:
			sprintf((char *)buffer, "bg_err_no_bonding");
			break;
		case bg_err_crypto:
			sprintf((char *)buffer, "bg_err_crypto");
			break;
		case bg_err_data_corrupted:
			sprintf((char *)buffer, "bg_err_data_corrupted");
			break;
		case bg_err_command_incomplete:
			sprintf((char *)buffer, "bg_err_command_incomplete");
			break;
		case bg_err_not_initialized:
			sprintf((char *)buffer, "bg_err_not_initialized");
			break;
		case bg_err_invalid_sync_handle:
			sprintf((char *)buffer, "bg_err_invalid_sync_handle");
			break;
		case bg_err_smp_passkey_entry_failed:
			sprintf((char *)buffer, "bg_err_smp_passkey_entry_failed");
			break;
		case bg_err_smp_oob_not_available:
			sprintf((char *)buffer, "bg_err_smp_oob_not_available");
			break;
		case bg_err_smp_authentication_requirements:
			sprintf((char *)buffer, "bg_err_smp_authentication_requirements");
			break;
		case bg_err_smp_confirm_value_failed:
			sprintf((char *)buffer, "bg_err_smp_confirm_value_failed");
			break;
		case bg_err_smp_pairing_not_supported:
			sprintf((char *)buffer, "bg_err_smp_pairing_not_supported");
			break;
		case bg_err_smp_encryption_key_size:
			sprintf((char *)buffer, "bg_err_smp_encryption_key_size");
			break;
		case bg_err_smp_command_not_supported:
			sprintf((char *)buffer, "bg_err_smp_command_not_supported");
			break;
		case bg_err_smp_unspecified_reason:
			sprintf((char *)buffer, "bg_err_smp_unspecified_reason");
			break;
		case bg_err_smp_repeated_attempts:
			sprintf((char *)buffer, "bg_err_smp_repeated_attempts");
			break;
		case bg_err_smp_invalid_parameters:
			sprintf((char *)buffer, "bg_err_smp_invalid_parameters");
			break;
		case bg_err_smp_dhkey_check_failed:
			sprintf((char *)buffer, "bg_err_smp_dhkey_check_failed");
			break;
		case bg_err_smp_numeric_comparison_failed:
			sprintf((char *)buffer, "bg_err_smp_numeric_comparison_failed");
			break;
		case bg_err_smp_bredr_pairing_in_progress:
			sprintf((char *)buffer, "bg_err_smp_bredr_pairing_in_progress");
			break;
		case bg_err_smp_cross_transport_key_derivation_generation_not_allowed:
			sprintf((char *)buffer, "bg_err_smp_cross_transport_key_derivation_generation_not_allowed");
			break;
		case bg_err_bt_unknown_connection_identifier:
			sprintf((char *)buffer, "bg_err_bt_unknown_connection_identifier");
			break;
		case bg_err_bt_authentication_failure:
			sprintf((char *)buffer, "bg_err_bt_authentication_failure");
			break;
		case bg_err_bt_pin_or_key_missing:
			sprintf((char *)buffer, "bg_err_bt_pin_or_key_missing");
			break;
		case bg_err_bt_memory_capacity_exceeded:
			sprintf((char *)buffer, "bg_err_bt_memory_capacity_exceeded");
			break;
		case bg_err_bt_connection_timeout:
			sprintf((char *)buffer, "bg_err_bt_connection_timeout");
			break;
		case bg_err_bt_connection_limit_exceeded:
			sprintf((char *)buffer, "bg_err_bt_connection_limit_exceeded");
			break;
		case bg_err_bt_synchronous_connectiontion_limit_exceeded:
			sprintf((char *)buffer, "bg_err_bt_synchronous_connectiontion_limit_exceeded");
			break;
		case bg_err_bt_acl_connection_already_exists:
			sprintf((char *)buffer, "bg_err_bt_acl_connection_already_exists");
			break;
		case bg_err_bt_command_disallowed:
			sprintf((char *)buffer, "bg_err_bt_command_disallowed");
			break;
		case bg_err_bt_connection_rejected_due_to_limited_resources:
			sprintf((char *)buffer, "bg_err_bt_connection_rejected_due_to_limited_resources");
			break;
		case bg_err_bt_connection_rejected_due_to_security_reasons:
			sprintf((char *)buffer, "bg_err_bt_connection_rejected_due_to_security_reasons");
			break;
		case bg_err_bt_connection_rejected_due_to_unacceptable_bd_addr:
			sprintf((char *)buffer, "bg_err_bt_connection_rejected_due_to_unacceptable_bd_addr");
			break;
		case bg_err_bt_connection_accept_timeout_exceeded:
			sprintf((char *)buffer, "bg_err_bt_connection_accept_timeout_exceeded");
			break;
		case bg_err_bt_unsupported_feature_or_parameter_value:
			sprintf((char *)buffer, "bg_err_bt_unsupported_feature_or_parameter_value");
			break;
		case bg_err_bt_invalid_command_parameters:
			sprintf((char *)buffer, "bg_err_bt_invalid_command_parameters");
			break;
		case bg_err_bt_remote_user_terminated:
			sprintf((char *)buffer, "bg_err_bt_remote_user_terminated");
			break;
		case bg_err_bt_remote_device_terminated_connection_due_to_low_resources:
			sprintf((char *)buffer, "bg_err_bt_remote_device_terminated_connection_due_to_low_resources");
			break;
		case bg_err_bt_remote_powering_off:
			sprintf((char *)buffer, "bg_err_bt_remote_powering_off");
			break;
		case bg_err_bt_connection_terminated_by_local_host:
			sprintf((char *)buffer, "bg_err_bt_connection_terminated_by_local_host");
			break;
		case bg_err_bt_repeated_attempts:
			sprintf((char *)buffer, "bg_err_bt_repeated_attempts");
			break;
		case bg_err_bt_pairing_not_allowed:
			sprintf((char *)buffer, "bg_err_bt_pairing_not_allowed");
			break;
		case bg_err_bt_unsupported_remote_feature:
			sprintf((char *)buffer, "bg_err_bt_unsupported_remote_feature");
			break;
		case bg_err_bt_unspecified_error:
			sprintf((char *)buffer, "bg_err_bt_unspecified_error");
			break;
		case bg_err_bt_ll_response_timeout:
			sprintf((char *)buffer, "bg_err_bt_ll_response_timeout");
			break;
		case bg_err_bt_ll_procedure_collision:
			sprintf((char *)buffer, "bg_err_bt_ll_procedure_collision");
			break;
		case bg_err_bt_encryption_mode_not_acceptable:
			sprintf((char *)buffer, "bg_err_bt_encryption_mode_not_acceptable");
			break;
		case bg_err_bt_link_key_cannot_be_changed:
			sprintf((char *)buffer, "bg_err_bt_link_key_cannot_be_changed");
			break;
		case bg_err_bt_instant_passed:
			sprintf((char *)buffer, "bg_err_bt_instant_passed");
			break;
		case bg_err_bt_pairing_with_unit_key_not_supported:
			sprintf((char *)buffer, "bg_err_bt_pairing_with_unit_key_not_supported");
			break;
		case bg_err_bt_different_transaction_collision:
			sprintf((char *)buffer, "bg_err_bt_different_transaction_collision");
			break;
		case bg_err_bt_channel_assessment_not_supported:
			sprintf((char *)buffer, "bg_err_bt_channel_assessment_not_supported");
			break;
		case bg_err_bt_insufficient_security:
			sprintf((char *)buffer, "bg_err_bt_insufficient_security");
			break;
		case bg_err_bt_parameter_out_of_mandatory_range:
			sprintf((char *)buffer, "bg_err_bt_parameter_out_of_mandatory_range");
			break;
		case bg_err_bt_simple_pairing_not_supported_by_host:
			sprintf((char *)buffer, "bg_err_bt_simple_pairing_not_supported_by_host");
			break;
		case bg_err_bt_host_busy_pairing:
			sprintf((char *)buffer, "bg_err_bt_host_busy_pairing");
			break;
		case bg_err_bt_connection_rejected_due_to_no_suitable_channel_found:
			sprintf((char *)buffer, "bg_err_bt_connection_rejected_due_to_no_suitable_channel_found");
			break;
		case bg_err_bt_controller_busy:
			sprintf((char *)buffer, "bg_err_bt_controller_busy");
			break;
		case bg_err_bt_unacceptable_connection_interval:
			sprintf((char *)buffer, "bg_err_bt_unacceptable_connection_interval");
			break;
		case bg_err_bt_advertising_timeout:
			sprintf((char *)buffer, "bg_err_bt_advertising_timeout");
			break;
		case bg_err_bt_connection_terminated_due_to_mic_failure:
			sprintf((char *)buffer, "bg_err_bt_connection_terminated_due_to_mic_failure");
			break;
		case bg_err_bt_connection_failed_to_be_established:
			sprintf((char *)buffer, "bg_err_bt_connection_failed_to_be_established");
			break;
		case bg_err_bt_mac_connection_failed:
			sprintf((char *)buffer, "bg_err_bt_mac_connection_failed");
			break;
		case bg_err_bt_coarse_clock_adjustment_rejected_but_will_try_to_adjust_using_clock_dragging:
			sprintf((char *)buffer, "bg_err_bt_coarse_clock_adjustment_rejected_but_will_try_to_adjust_using_clock_dragging=bg_errspc_bt+64");
			break;
		case bg_err_bt_unknown_advertising_identifier:
			sprintf((char *)buffer, "bg_err_bt_unknown_advertising_identifier");
			break;
		case bg_err_bt_limit_reached:
			sprintf((char *)buffer, "bg_err_bt_limit_reached");
			break;
		case bg_err_bt_operation_cancelled_by_host:
			sprintf((char *)buffer, "bg_err_bt_operation_cancelled_by_host");
			break;
		case bg_err_bt_packet_too_long:
			sprintf((char *)buffer, "bg_err_bt_packet_too_long");
			break;
		case bg_err_application_file_open_failed:
			sprintf((char *)buffer, "bg_err_application_file_open_failed");
			break;
		case bg_err_application_xml_parse_failed:
			sprintf((char *)buffer, "bg_err_application_xml_parse_failed");
			break;
		case bg_err_application_device_connection_failed:
			sprintf((char *)buffer, "bg_err_application_device_connection_failed");
			break;
		case bg_err_application_device_comunication_failed:
			sprintf((char *)buffer, "bg_err_application_device_comunication_failed");
			break;
		case bg_err_application_authentication_failed:
			sprintf((char *)buffer, "bg_err_application_authentication_failed");
			break;
		case bg_err_application_incorrect_gatt_database:
			sprintf((char *)buffer, "bg_err_application_incorrect_gatt_database");
			break;
		case bg_err_application_disconnected_due_to_procedure_collision:
			sprintf((char *)buffer, "bg_err_application_disconnected_due_to_procedure_collision");
			break;
		case bg_err_application_disconnected_due_to_secure_session_failed:
			sprintf((char *)buffer, "bg_err_application_disconnected_due_to_secure_session_failed");
			break;
		case bg_err_application_encryption_decryption_error:
			sprintf((char *)buffer, "bg_err_application_encryption_decryption_error");
			break;
		case bg_err_application_maximum_retries:
			sprintf((char *)buffer, "bg_err_application_maximum_retries");
			break;
		case bg_err_application_data_parse_failed:
			sprintf((char *)buffer, "bg_err_application_data_parse_failed");
			break;
		case bg_err_application_pairing_removed:
			sprintf((char *)buffer, "bg_err_application_pairing_removed");
			break;
		case bg_err_application_inactive_timeout:
			sprintf((char *)buffer, "bg_err_application_inactive_timeout");
			break;
		case bg_err_application_mismatched_or_insufficient_security:
			sprintf((char *)buffer, "bg_err_application_mismatched_or_insufficient_security");
			break;
		case bg_err_att_invalid_handle:
			sprintf((char *)buffer, "bg_err_att_invalid_handle");
			break;
		case bg_err_att_read_not_permitted:
			sprintf((char *)buffer, "bg_err_att_read_not_permitted");
			break;
		case bg_err_att_write_not_permitted:
			sprintf((char *)buffer, "bg_err_att_write_not_permitted");
			break;
		case bg_err_att_invalid_pdu:
			sprintf((char *)buffer, "bg_err_att_invalid_pdu");
			break;
		case bg_err_att_insufficient_authentication:
			sprintf((char *)buffer, "bg_err_att_insufficient_authentication");
			break;
		case bg_err_att_request_not_supported:
			sprintf((char *)buffer, "bg_err_att_request_not_supported");
			break;
		case bg_err_att_invalid_offset:
			sprintf((char *)buffer, "bg_err_att_invalid_offset");
			break;
		case bg_err_att_insufficient_authorization:
			sprintf((char *)buffer, "bg_err_att_insufficient_authorization");
			break;
		case bg_err_att_prepare_queue_full:
			sprintf((char *)buffer, "bg_err_att_prepare_queue_full");
			break;
		case bg_err_att_att_not_found:
			sprintf((char *)buffer, "bg_err_att_att_not_found");
			break;
		case bg_err_att_att_not_long:
			sprintf((char *)buffer, "bg_err_att_att_not_long");
			break;
		case bg_err_att_insufficient_enc_key_size:
			sprintf((char *)buffer, "bg_err_att_insufficient_enc_key_size");
			break;
		case bg_err_att_invalid_att_length:
			sprintf((char *)buffer, "bg_err_att_invalid_att_length");
			break;
		case bg_err_att_unlikely_error:
			sprintf((char *)buffer, "bg_err_att_unlikely_error");
			break;
		case bg_err_att_insufficient_encryption:
			sprintf((char *)buffer, "bg_err_att_insufficient_encryption");
			break;
		case bg_err_att_unsupported_group_type:
			sprintf((char *)buffer, "bg_err_att_unsupported_group_type");
			break;
		case bg_err_att_insufficient_resources:
			sprintf((char *)buffer, "bg_err_att_insufficient_resources");
			break;
		case bg_err_att_out_of_sync:
			sprintf((char *)buffer, "bg_err_att_out_of_sync");
			break;
		case bg_err_att_value_not_allowed:
			sprintf((char *)buffer, "bg_err_att_value_not_allowed");
			break;
		case bg_err_att_application:
			sprintf((char *)buffer, "bg_err_att_application");
			break;
		case bg_err_mesh_already_exists:
			sprintf((char *)buffer, "bg_err_mesh_already_exists");
			break;
		case bg_err_mesh_does_not_exist:
			sprintf((char *)buffer, "bg_err_mesh_does_not_exist");
			break;
		case bg_err_mesh_limit_reached:
			sprintf((char *)buffer, "bg_err_mesh_limit_reached");
			break;
		case bg_err_mesh_invalid_address:
			sprintf((char *)buffer, "bg_err_mesh_invalid_address");
			break;
		case bg_err_mesh_malformed_data:
			sprintf((char *)buffer, "bg_err_mesh_malformed_data");
			break;
		case bg_err_mesh_already_initialized:
			sprintf((char *)buffer, "bg_err_mesh_already_initialized");
			break;
		case bg_err_mesh_not_initialized:
			sprintf((char *)buffer, "bg_err_mesh_not_initialized");
			break;
		case bg_err_mesh_no_friend_offer:
			sprintf((char *)buffer, "bg_err_mesh_no_friend_offer");
			break;
		case bg_err_mesh_prov_link_closed:
			sprintf((char *)buffer, "bg_err_mesh_prov_link_closed");
			break;
		case bg_err_mesh_prov_invalid_pdu:
			sprintf((char *)buffer, "bg_err_mesh_prov_invalid_pdu");
			break;
		case bg_err_mesh_prov_invalid_pdu_format:
			sprintf((char *)buffer, "bg_err_mesh_prov_invalid_pdu_format");
			break;
		case bg_err_mesh_prov_unexpected_pdu:
			sprintf((char *)buffer, "bg_err_mesh_prov_unexpected_pdu");
			break;
		case bg_err_mesh_prov_confirmation_failed:
			sprintf((char *)buffer, "bg_err_mesh_prov_confirmation_failed");
			break;
		case bg_err_mesh_prov_out_of_resources:
			sprintf((char *)buffer, "bg_err_mesh_prov_out_of_resources");
			break;
		case bg_err_mesh_prov_decryption_failed:
			sprintf((char *)buffer, "bg_err_mesh_prov_decryption_failed");
			break;
		case bg_err_mesh_prov_unexpected_error:
			sprintf((char *)buffer, "bg_err_mesh_prov_unexpected_error");
			break;
		case bg_err_mesh_prov_cannot_assign_addr:
			sprintf((char *)buffer, "bg_err_mesh_prov_cannot_assign_addr");
			break;
		case bg_err_mesh_address_temporarily_unavailable:
			sprintf((char *)buffer, "bg_err_mesh_address_temporarily_unavailable");
			break;
		case bg_err_mesh_address_already_used:
			sprintf((char *)buffer, "bg_err_mesh_address_already_used");
			break;
		case bg_err_mesh_foundation_invalid_address:
			sprintf((char *)buffer, "bg_err_mesh_foundation_invalid_address");
			break;
		case bg_err_mesh_foundation_invalid_model:
			sprintf((char *)buffer, "bg_err_mesh_foundation_invalid_model");
			break;
		case bg_err_mesh_foundation_invalid_app_key:
			sprintf((char *)buffer, "bg_err_mesh_foundation_invalid_app_key");
			break;
		case bg_err_mesh_foundation_invalid_net_key:
			sprintf((char *)buffer, "bg_err_mesh_foundation_invalid_net_key");
			break;
		case bg_err_mesh_foundation_insufficient_resources:
			sprintf((char *)buffer, "bg_err_mesh_foundation_insufficient_resources");
			break;
		case bg_err_mesh_foundation_key_index_exists:
			sprintf((char *)buffer, "bg_err_mesh_foundation_key_index_exists");
			break;
		case bg_err_mesh_foundation_invalid_publish_params:
			sprintf((char *)buffer, "bg_err_mesh_foundation_invalid_publish_params");
			break;
		case bg_err_mesh_foundation_not_subscribe_model:
			sprintf((char *)buffer, "bg_err_mesh_foundation_not_subscribe_model");
			break;
		case bg_err_mesh_foundation_storage_failure:
			sprintf((char *)buffer, "bg_err_mesh_foundation_storage_failure");
			break;
		case bg_err_mesh_foundation_not_supported:
			sprintf((char *)buffer, "bg_err_mesh_foundation_not_supported");
			break;
		case bg_err_mesh_foundation_cannot_update:
			sprintf((char *)buffer, "bg_err_mesh_foundation_cannot_update");
			break;
		case bg_err_mesh_foundation_cannot_remove:
			sprintf((char *)buffer, "bg_err_mesh_foundation_cannot_remove");
			break;
		case bg_err_mesh_foundation_cannot_bind:
			sprintf((char *)buffer, "bg_err_mesh_foundation_cannot_bind");
			break;
		case bg_err_mesh_foundation_temporarily_unable:
			sprintf((char *)buffer, "bg_err_mesh_foundation_temporarily_unable");
			break;
		case bg_err_mesh_foundation_cannot_set:
			sprintf((char *)buffer, "bg_err_mesh_foundation_cannot_set");
			break;
		case bg_err_mesh_foundation_unspecified:
			sprintf((char *)buffer, "bg_err_mesh_foundation_unspecified");
			break;
		case bg_err_mesh_foundation_invalid_binding:
			sprintf((char *)buffer, "bg_err_mesh_foundation_invalid_binding");
			break;
		case bg_err_filesystem_file_not_found:
			sprintf((char *)buffer, "bg_err_filesystem_file_not_found");
			break;
		case bg_err_l2cap_remote_disconnected:
			sprintf((char *)buffer, "bg_err_l2cap_remote_disconnected");
			break;
		case bg_err_l2cap_local_disconnected:
			sprintf((char *)buffer, "bg_err_l2cap_local_disconnected");
			break;
		case bg_err_l2cap_cid_not_exist:
			sprintf((char *)buffer, "bg_err_l2cap_cid_not_exist");
			break;
		case bg_err_l2cap_le_disconnected:
			sprintf((char *)buffer, "bg_err_l2cap_le_disconnected");
			break;
		case bg_err_l2cap_flow_control_violated:
			sprintf((char *)buffer, "bg_err_l2cap_flow_control_violated");
			break;
		case bg_err_l2cap_flow_control_credit_overflowed:
			sprintf((char *)buffer, "bg_err_l2cap_flow_control_credit_overflowed");
			break;
		case bg_err_l2cap_no_flow_control_credit:
			sprintf((char *)buffer, "bg_err_l2cap_no_flow_control_credit");
			break;
		case bg_err_l2cap_connection_request_timeout:
			sprintf((char *)buffer, "bg_err_l2cap_connection_request_timeout");
			break;
		case bg_err_l2cap_invalid_cid:
			sprintf((char *)buffer, "bg_err_l2cap_invalid_cid");
			break;
		case bg_err_l2cap_wrong_state:
			sprintf((char *)buffer, "bg_err_l2cap_wrong_state");
			break;
		case bg_err_security_image_signature_verification_failed:
			sprintf((char *)buffer, "bg_err_security_image_signature_verification_failed");
			break;
		case bg_err_security_file_signature_verification_failed:
			sprintf((char *)buffer, "bg_err_security_file_signature_verification_failed");
			break;
		case bg_err_security_image_checksum_error:
			sprintf((char *)buffer, "bg_err_security_image_checksum_error");
			break;
		case bg_err_last:
			sprintf((char *)buffer, "bg_err_last");
			break;
	}
#endif

	return buffer;
}
