//-----------------------------------------------------------------------------
//   All Rights Reserved
//  Intermatic Incorporated
//  Copyright 2019
//-----------------------------------------------------------------------------
//
//  Description: mesh_model_common.h contains constants, macros, structure
//  definitions, and public prototypes that are common to all models
//
// 	Here is where you would put a function common to more than one models
//
//-----------------------------------------------------------------------------

#ifndef _MESH_MODEL_COMMON_H_
#define _MESH_MODEL_COMMON_H_

#include "bg_types.h"
#include "bg_errorcodes.h"

// #defines and typedefs
#define LIGHT_LC_SERVER_PRESENT 1
#define GENERIC_DEFAULT_TRANSITION_SERVER_PRESENT 1

#define MAX_TIMER_24_TIME 0x00ffffff

// global functions
extern uint8_t* get_api_error_text(errorcode_t error_code);

#endif // _MESH_MODEL_COMMON_H_
