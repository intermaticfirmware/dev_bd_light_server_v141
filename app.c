#include "app.h"
#include "stdbool.h"
#include "bg_types.h"
#include "native_gecko.h"
#include "log.h"
#include "gatt_db.h"
#include "em_gpio.h"
#include "em_rtcc.h"
#include "vendor_sensor_model_def.h"
#include "mesh_generic_model_capi_types.h"
#include "mesh_model_common.h"

extern uint8_t boot_to_dfu;
/***********************************************************************************************//**
 * @addtogroup app
 * @{
 **************************************************************************************************/
#define TIMER_ID_FACTORY_RESET  77
#define TIMER_ID_PERIODICAL_UPDATE						50

#define TIMER_ID_RESET 10

#define EX_PB0_PRESS									((1) << 5)
#define EX_PB1_PRESS									((1) << 6)

#define RES_100_MILLI_TICKS				3277
#define RES_1_SEC_TICKS					(32768)
#define RES_10_SEC_TICKS				((32768)*(10))
#define RES_10_MIN_TICKS				((32768)*(60)*(10))

#define RES_100_MILLI				0
#define RES_1_SEC					((1) << 6)
#define RES_10_SEC					((2) << 6)
#define RES_10_MIN					((3) << 6)

#define RES_BIT_MASK			0xC0

#define SIG_MODEL_ID            0xFFFF
#define GENERIC_ON_OFF_GRP_ADR  0xC001

uint8_t occ_sensor_state[OCC_DATA_LENGTH];
uint8_t temperature[TEMP_DATA_LENGTH];

uint8_t sensor_settings[SENSOR_SETTING_DATA_LENGTH];
uint8_t update_interval[UPDATE_INTERVAL_LENGTH] = {
		0 };

uint32_t real_time_ticks = 0;
uint8_t conn_handle = 0xFF;

// default settings used on initialization
sensor_settings_t thresh_sensor_msg_settings = {
		.sensor_property_id = AMBIENT_LIGHT_PROPERTY_ID,
		.sensor_setting_property_id = THRESHOLD_MSG_ID,
		.setting = 100
};

sensor_settings_t thresh_sensor_sleep_settings = {
		.sensor_property_id = AMBIENT_LIGHT_PROPERTY_ID,
		.sensor_setting_property_id = THRESHOLD_SLEEP_ID,
		.setting = 50
};


uint8_t light_status[3];

light_sensor_msg_t light_struct_status;

light_vendor_model_t light_sensor_server = {
		.elem_index = PRIMARY_ELEMENT,
		.vendor_id = SILABS_VENDOR_ID,
		.model_id = VENDOR_SENSOR_SERVER_LIGHT_ID,
		.publish = 1,
		.opcodes_len = LIGHT_VEND_NUMBER_OF_OPCODES,
		.opcodes_data[0] = sensor_get,
		.opcodes_data[1] = sensor_settings_set,
		.opcodes_data[2] = sensor_status,
		.opcodes_data[3] = sensor_setting_status,
		.opcodes_data[4] = sensor_cadence_get,
		.opcodes_data[5] = sensor_cadence_set,
		.opcodes_data[6] = sensor_cadence_set_unack,
		.opcodes_data[7] = sensor_cadence_status
};

static void handle_gecko_event(uint32_t evt_id, struct gecko_cmd_packet *evt);
bool mesh_bgapi_listener(struct gecko_cmd_packet *evt);


void key_interrupt_handler(void) {
	uint32_t int_flag = GPIO_IntGet();
	GPIO_IntClear(int_flag);
	if (int_flag & (1 << BSP_BUTTON0_PIN)) {
		gecko_external_signal(EX_PB0_PRESS);
	}
	if (int_flag & (1 << BSP_BUTTON1_PIN)) {
		gecko_external_signal(EX_PB1_PRESS);
	}
}
void GPIO_EVEN_IRQHandler(void) {
	key_interrupt_handler();
}
void GPIO_ODD_IRQHandler(void) {
	key_interrupt_handler();
}

void initiate_factory_reset(void) {
	/* if connection is open then close it before rebooting */
	if (conn_handle != 0xFF) {
		gecko_cmd_le_connection_close(conn_handle);
	}

	/* perform a factory reset by erasing PS storage. This removes all the keys and other settings
	 that have been configured for this node */
	gecko_cmd_flash_ps_erase_all();
	// reboot after a small delay
	gecko_cmd_hardware_set_soft_timer(2 * 32768, TIMER_ID_FACTORY_RESET, 1);
}


static void parse_period(uint8_t interval) {
	switch (interval & RES_BIT_MASK) {
	case RES_100_MILLI:
		real_time_ticks = RES_100_MILLI_TICKS * (interval & (~RES_BIT_MASK));
		break;
	case RES_1_SEC:
		real_time_ticks = RES_1_SEC_TICKS * (interval & (~RES_BIT_MASK));
		break;
	case RES_10_SEC:
		real_time_ticks = RES_10_SEC_TICKS * (interval & (~RES_BIT_MASK));
		break;
	case RES_10_MIN:
		real_time_ticks = RES_10_MIN_TICKS * (interval & (~RES_BIT_MASK));
		break;
	default:
		break;
	}
}

void AppHandler(void){
	INIT_LOG();
	GPIO_PinModeSet(BSP_BUTTON1_PORT, BSP_BUTTON1_PIN, gpioModeInputPull, 1);
	GPIO_PinModeSet(BSP_BUTTON0_PORT, BSP_BUTTON0_PIN, gpioModeInputPull, 1);

	NVIC_EnableIRQ(GPIO_EVEN_IRQn);
	NVIC_EnableIRQ(GPIO_ODD_IRQn);

	GPIO_IntConfig(BSP_BUTTON0_PORT, BSP_BUTTON0_PIN, true, false, true);
	GPIO_IntConfig(BSP_BUTTON1_PORT, BSP_BUTTON1_PIN, true, false, true);

	while (1) {
		struct gecko_cmd_packet *evt = gecko_wait_event();
		bool pass = mesh_bgapi_listener(evt);
		if (pass) {
			handle_gecko_event(BGLIB_MSG_ID(evt->header), evt);
		}
	}
}

static void handle_gecko_event(uint32_t evt_id, struct gecko_cmd_packet *evt) {
    uint16_t temp_result;
	switch (evt_id) {

	case gecko_evt_hardware_soft_timer_id:
		switch (evt->data.evt_hardware_soft_timer.handle) {
		case TIMER_ID_FACTORY_RESET:
			LOGW("Software reset.\r\n");
			gecko_cmd_system_reset(0);
			break;
		case TIMER_ID_RESET:
			LOGW("Software reset.\r\n");
			gecko_cmd_system_reset(0);
			break;
		}
		break;

		case gecko_evt_le_connection_opened_id:
			LOGI("Connected.\r\n");
			conn_handle = evt->data.evt_le_connection_opened.connection;
			break;

		case gecko_evt_system_boot_id:
			if (GPIO_PinInGet(BSP_BUTTON1_PORT, BSP_BUTTON1_PIN) == 0) {
				LOGW("Factory Reset.\r\n");
				initiate_factory_reset();
			} else {
				LOGI("Normal start.\r\n");
				// Initialize Mesh stack in Node operation mode, wait for initialized event
				gecko_cmd_mesh_node_init();
			}

			break;

		case gecko_evt_mesh_vendor_model_receive_id: {
			struct gecko_msg_mesh_vendor_model_receive_evt_t *recv_evt = (struct gecko_msg_mesh_vendor_model_receive_evt_t *) &evt->data;
			struct gecko_msg_mesh_vendor_model_send_rsp_t* tx_ret;
			struct gecko_msg_mesh_vendor_model_set_publication_rsp_t *set_pub_ret;
			struct gecko_msg_mesh_vendor_model_publish_rsp_t *pub_ret;
			uint8_t action_req = 0, opcode = 0, payload_len = 0, *payload_data;
			LOGD("Vendor model data received.\r\n\tElement index = %d\r\n\tVendor id = 0x%04X\r\n\tModel id = 0x%04X\r\n\tSource address = 0x%04X\r\n\tDestination address = 0x%04X\r\n\tDestination label UUID index = 0x%02X\r\n\tApp key index = 0x%04X\r\n\tNon-relayed = 0x%02X\r\n\tOpcode = 0x%02X\r\n\tFinal = 0x%04X\r\n\tPayload: ", recv_evt->elem_index, recv_evt->vendor_id, recv_evt->model_id, recv_evt->source_address, recv_evt->destination_address, recv_evt->va_index, recv_evt->appkey_index, recv_evt->nonrelayed, recv_evt->opcode, recv_evt->final);
			UINT8_ARRAY_DUMP(recv_evt->payload.data, recv_evt->payload.len);
			LOGN();

			switch (recv_evt->opcode) {
			// Server
			case sensor_get:
				LOGI("Sending/publishing light sensor status as response to sensor get from client...\r\n");
				light_struct_status.header.format = MPID_HEADER_B;
				light_struct_status.header.length = sizeof(light_status);
				light_struct_status.header.property_id = AMBIENT_LIGHT_PROPERTY_ID;
				light_struct_status.sensor_data[2] = light_status[2];
				light_struct_status.sensor_data[1] = light_status[1];
				light_struct_status.sensor_data[0] = light_status[0];

				action_req = ACK_REQ | STATUS_UPDATE_REQ;
				opcode = sensor_status;
				payload_len = sizeof(light_sensor_msg_t);
				payload_data = (uint8_t*)&light_struct_status;
				break;
				// Server
			case sensor_settings_set:
			{
				uint16 setting_propery_id;
				setting_propery_id = recv_evt->payload.data[3]<<8|recv_evt->payload.data[2];;
				switch (setting_propery_id)
				{
				case THRESHOLD_MSG_ID:
					memcpy(&thresh_sensor_msg_settings, &recv_evt->payload.data[0], recv_evt->payload.len);
					payload_len = recv_evt->payload.len;
					payload_data = (uint8_t*)&thresh_sensor_msg_settings;
					break;
				case THRESHOLD_SLEEP_ID:
					memcpy(&thresh_sensor_sleep_settings, &recv_evt->payload.data[0], recv_evt->payload.len);
					payload_len = recv_evt->payload.len;
					payload_data = (uint8_t*)&thresh_sensor_sleep_settings;
					break;
				}

				LOGI("Sending/publishing sensor setting status as response to sensor settings set from client...\r\n");
				action_req = ACK_REQ | STATUS_UPDATE_REQ;
				opcode = sensor_setting_status;
			}
				break;
			default:
				break;
			}

			if (action_req & ACK_REQ) {
				// send back to node
				tx_ret = gecko_cmd_mesh_vendor_model_send(light_sensor_server.elem_index, light_sensor_server.vendor_id, light_sensor_server.model_id, recv_evt->source_address, recv_evt->va_index, recv_evt->appkey_index, recv_evt->nonrelayed, opcode, 1, payload_len, payload_data);
				if (tx_ret->result) {
					LOGE("gecko_cmd_mesh_vendor_model_send error = 0x%04X\r\n", tx_ret->result);
					ERROR_ADDRESSING();
				} else {
					LOGD("gecko_cmd_mesh_vendor_model_send done.\r\n");
				}
			}
			if (action_req & STATUS_UPDATE_REQ) {
				// publish to address setup for light_sensor_server model
				set_pub_ret = gecko_cmd_mesh_vendor_model_set_publication(light_sensor_server.elem_index, light_sensor_server.vendor_id, light_sensor_server.model_id, opcode, 1, payload_len, payload_data);
				if (set_pub_ret->result) {
					LOGE("Set publication error = 0x%04X\r\n", set_pub_ret->result);
					ERROR_ADDRESSING();
				} else {
					LOGD("Set publication done. Publishing...\r\n");
					pub_ret = gecko_cmd_mesh_vendor_model_publish(light_sensor_server.elem_index, light_sensor_server.vendor_id, light_sensor_server.model_id);
					if (pub_ret->result) {
						LOGE("Publish error = 0x%04X\r\n", pub_ret->result);
						ERROR_ADDRESSING();
					} else {
						LOGD("Publish done.\r\n");
					}
				}
			}
		}
		break;

		case gecko_evt_system_external_signal_id: {
			uint8_t opcode = 0, length = 0, *data = NULL;
			struct gecko_msg_mesh_vendor_model_set_publication_rsp_t *set_pub_ret;
			struct gecko_msg_mesh_vendor_model_publish_rsp_t *pub_ret;
			if (evt->data.evt_system_external_signal.extsignals & EX_PB0_PRESS) {
				light_status[0] = 0; // Pushbutton 0 set to 0x00 0x00 0x00
				light_status[1] = 0;
				light_status[2] = 0;
				light_struct_status.header.format = MPID_HEADER_B;
				light_struct_status.header.length = sizeof(light_status);
				light_struct_status.header.property_id = AMBIENT_LIGHT_PROPERTY_ID;
				light_struct_status.sensor_data[0] = light_status[0];
				light_struct_status.sensor_data[1] = light_status[1];
				light_struct_status.sensor_data[2] = light_status[2];
				opcode = sensor_status;
				length = sizeof(light_sensor_msg_t);
				data = (uint8_t*)&light_struct_status;
				LOGD("PB0 Pressed. - sensor_status light = 0x00 0x00 0x00\r\n");
			}
			if (evt->data.evt_system_external_signal.extsignals & EX_PB1_PRESS) {
				light_status[0] = 0x54; // Pushbutton 1 set to 0x55 0xAA 0x55
				light_status[1] = 0xAA;
				light_status[2] = 0x55;
				light_struct_status.header.format = MPID_HEADER_B;
				light_struct_status.header.length = sizeof(light_status);
				light_struct_status.header.property_id = AMBIENT_LIGHT_PROPERTY_ID;
				light_struct_status.sensor_data[0] = light_status[0];
				light_struct_status.sensor_data[1] = light_status[1];
				light_struct_status.sensor_data[2] = light_status[2];
				opcode = sensor_status;
				length = sizeof(light_sensor_msg_t);
				data = (uint8_t*)&light_struct_status;
				LOGD("PB1 Pressed. - sensor_status light = 0x55 0xAA 0x54\r\n");
			}

			set_pub_ret = gecko_cmd_mesh_vendor_model_set_publication(light_sensor_server.elem_index, light_sensor_server.vendor_id, light_sensor_server.model_id, opcode, 1, length, data);
			if (set_pub_ret->result) {
				LOGE("Set publication error = 0x%04X\r\n", set_pub_ret->result);
				ERROR_ADDRESSING();
			} else {
				LOGD("Set publication done. Publishing...\r\n");
				pub_ret = gecko_cmd_mesh_vendor_model_publish(light_sensor_server.elem_index, light_sensor_server.vendor_id, light_sensor_server.model_id);
				if (pub_ret->result) {
					LOGE("Publish error = 0x%04X\r\n", pub_ret->result);
					ERROR_ADDRESSING();
				} else {
					LOGD("Publish done.\r\n");
				}
			}
		}
		break;
//#define	SELF_PROVISION_ENABLED 1

		case gecko_evt_mesh_node_initialized_id: {
			struct gecko_msg_mesh_node_initialized_evt_t *node_init_ret = (struct gecko_msg_mesh_node_initialized_evt_t *) &evt->data;
			struct gecko_msg_mesh_vendor_model_init_rsp_t *vm_init_ret;
#if (SELF_PROVISION_ENABLED)
			static aes_key_128 enc_key = {.data = "\x03\x03\x03\x03\x03\x03\x03\x03\x03\x03\x03\x03\x03\x03\x03\x03"};
#endif
			vm_init_ret = gecko_cmd_mesh_vendor_model_init(light_sensor_server.elem_index, light_sensor_server.vendor_id, light_sensor_server.model_id, light_sensor_server.publish, light_sensor_server.opcodes_len, light_sensor_server.opcodes_data);
			if (vm_init_ret->result) {
				LOGE("Vendor model init error = 0x%04X\r\n", vm_init_ret->result);
				ERROR_ADDRESSING();
			} else {
				LOGI("Vendor model init done. --- ");
				LOGW("Server. \r\n");
			}

			if (node_init_ret->provisioned) {
				// provisioned already
				LOGI("Provisioned already.\r\n");

#if (SELF_PROVISION_ENABLED)
				struct gecko_msg_mesh_test_get_local_model_sub_rsp_t *sub_setting = gecko_cmd_mesh_test_get_local_model_sub(PRIMARY_ELEMENT, SIG_MODEL_ID, MESH_GENERIC_ON_OFF_SERVER_MODEL_ID);
				uint16_t sub_address = sub_setting->addresses.data[0] | (sub_setting->addresses.data[1] << 8);
				if (!sub_setting->result && (sub_address == GENERIC_ON_OFF_GRP_ADR))
				{
					LOGI("Configuration done already.\n\r");
				}
				else
				{
					// Setup models. This will only happen after a node is first provisioned
					temp_result = gecko_cmd_mesh_test_add_local_key(1, enc_key, 0, 0)->result;
					LOGI("gecko_cmd_mesh_test_add_local_key %s\n\r", get_api_error_text(temp_result));

					temp_result = gecko_cmd_mesh_test_bind_local_model_app(PRIMARY_ELEMENT, 0, SIG_MODEL_ID, MESH_GENERIC_ON_OFF_SERVER_MODEL_ID)->result;
					LOGI("gecko_cmd_mesh_test_bind_local_model_app %s\n\r", get_api_error_text(temp_result));

					temp_result = gecko_cmd_mesh_test_bind_local_model_app(PRIMARY_ELEMENT, 0, SILABS_VENDOR_ID, VENDOR_SENSOR_SERVER_LIGHT_ID)->result;
					LOGI("gecko_cmd_mesh_test_bind_local_model_app %s\n\r", get_api_error_text(temp_result));

					// additional setup normally done by provisioner
					temp_result = gecko_cmd_mesh_test_set_relay(1, 0, 0)->result;	// turn relay feature on
					LOGI("gecko_cmd_mesh_test_set_relay %s\n\r", get_api_error_text(temp_result));

					temp_result = gecko_cmd_mesh_test_set_nettx(2, 4)->result;
					LOGI("gecko_cmd_mesh_test_set_nettx %s\n\r", get_api_error_text(temp_result));

					// setup sub
					temp_result = gecko_cmd_mesh_test_add_local_model_sub(PRIMARY_ELEMENT, SIG_MODEL_ID, MESH_GENERIC_ON_OFF_SERVER_MODEL_ID, GENERIC_ON_OFF_GRP_ADR)->result;
					LOGI("gecko_cmd_mesh_test_add_local_model_sub %s\n\r", get_api_error_text(temp_result));

					temp_result = gecko_cmd_mesh_test_add_local_model_sub(PRIMARY_ELEMENT, SILABS_VENDOR_ID, VENDOR_SENSOR_SERVER_LIGHT_ID, VENDOR_SENSOR_LIGHT_GRP_ADDR)->result;
					LOGI("gecko_cmd_mesh_test_add_local_model_sub %s\n\r", get_api_error_text(temp_result));

					// setup pub
					temp_result = gecko_cmd_mesh_test_set_local_model_pub(PRIMARY_ELEMENT, 0, SIG_MODEL_ID, MESH_GENERIC_ON_OFF_SERVER_MODEL_ID, GENERIC_ON_OFF_GRP_ADR, 5, 0, 0, 0)->result;
					LOGI("gecko_cmd_mesh_test_set_local_model_pub %s\n\r", get_api_error_text(temp_result));

					temp_result = gecko_cmd_mesh_test_set_local_model_pub(PRIMARY_ELEMENT, 0, SILABS_VENDOR_ID, VENDOR_SENSOR_SERVER_LIGHT_ID, VENDOR_SENSOR_LIGHT_GRP_ADDR, 5, 0, 0, 0)->result;
					LOGI("gecko_cmd_mesh_test_add_local_model_sub %s\n\r", get_api_error_text(temp_result));
				}
#endif
			}
			else
			{
				LOGI("node is unprovisioned\r\n");
#if (SELF_PROVISION_ENABLED)
				LOGI("Self Provisioning...\r\n");
				struct gecko_msg_system_get_bt_address_rsp_t *bd_ret = gecko_cmd_system_get_bt_address();
				temp_result = gecko_cmd_mesh_node_set_provisioning_data(enc_key, enc_key, 0, 0, ((bd_ret->address.addr[1] << 8) | bd_ret->address.addr[0]) & 0x7FFF, 0)->result;

				LOGI("gecko_cmd_mesh_node_set_provisioning_data %s\n\r", get_api_error_text(temp_result));
				gecko_cmd_hardware_set_soft_timer(3208, TIMER_ID_RESET, 1);
#else
				// The Node is now initialized, start unprovisioned Beaconing using PB-Adv Bearer
				gecko_cmd_mesh_node_start_unprov_beaconing(0x3);
				LOGI("Unprovisioned beaconing.\r\n");
#endif
			}
		}
		break;

		case gecko_evt_mesh_node_provisioned_id: {
			LOGI("Provisioning done.\r\n");
		}
		break;

		case gecko_evt_mesh_node_provisioning_failed_id:
			LOGW("Provisioning failed. Result = 0x%04x\r\n", evt->data.evt_mesh_node_provisioning_failed.result);
			break;

		case gecko_evt_mesh_node_provisioning_started_id:
			LOGI("Provisioning started.\r\n");
			break;

		case gecko_evt_mesh_node_key_added_id:
			LOGI("got new %s key with index %x\r\n", evt->data.evt_mesh_node_key_added.type == 0 ? "network" : "application", evt->data.evt_mesh_node_key_added.index);
			break;

		case gecko_evt_mesh_node_config_set_id: {
			LOGD("gecko_evt_mesh_node_config_set_id\r\n\t");
			struct gecko_msg_mesh_node_config_set_evt_t *set_evt = (struct gecko_msg_mesh_node_config_set_evt_t *) &evt->data;
			UINT8_ARRAY_DUMP(set_evt->value.data, set_evt->value.len);
		}
		break;
		case gecko_evt_mesh_node_model_config_changed_id:
			LOGI("model config changed\r\n");
			break;

		case gecko_evt_le_connection_closed_id:
			LOGI("Disconnected.\r\n");
			conn_handle = 0xFF;
			/* Check if need to boot to dfu mode */
			if (boot_to_dfu) {
				/* Enter to DFU OTA mode */
				gecko_cmd_system_reset(2);
			}
			break;
		case gecko_evt_gatt_server_user_write_request_id:
			if (evt->data.evt_gatt_server_user_write_request.characteristic == gattdb_ota_control) {
				/* Set flag to enter to OTA mode */
				boot_to_dfu = 1;
				/* Send response to Write Request */
				gecko_cmd_gatt_server_send_user_write_response(evt->data.evt_gatt_server_user_write_request.connection,
						gattdb_ota_control, bg_err_success);

				/* Close connection to enter to DFU OTA mode */
				gecko_cmd_le_connection_close(evt->data.evt_gatt_server_user_write_request.connection);
			}
			break;
		default:
			break;
	}
}
