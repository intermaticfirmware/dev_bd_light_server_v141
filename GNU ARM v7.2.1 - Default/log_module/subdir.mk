################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../log_module/SEGGER_RTT.c \
../log_module/SEGGER_RTT_printf.c \
../log_module/log.c \
../log_module/retargetio.c \
../log_module/retargetserial.c 

OBJS += \
./log_module/SEGGER_RTT.o \
./log_module/SEGGER_RTT_printf.o \
./log_module/log.o \
./log_module/retargetio.o \
./log_module/retargetserial.o 

C_DEPS += \
./log_module/SEGGER_RTT.d \
./log_module/SEGGER_RTT_printf.d \
./log_module/log.d \
./log_module/retargetio.d \
./log_module/retargetserial.d 


# Each subdirectory must supply rules for building sources it contributes
log_module/SEGGER_RTT.o: ../log_module/SEGGER_RTT.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM C Compiler'
	arm-none-eabi-gcc -g -gdwarf-2 -mcpu=cortex-m4 -mthumb -std=c99 '-D__HEAP_SIZE=0x1200' '-D__STACK_SIZE=0x1000' '-DHAL_CONFIG=1' '-DMESH_LIB_NATIVE=1' '-DEFR32BG13P632F512GM48=1' -I"C:\git\dev_bd_light_server_v141" -I"C:\git\dev_bd_light_server_v141\log_module" -I"C:\git\dev_bd_light_server_v141\protocol\bluetooth\bt_mesh\inc\common" -I"C:\git\dev_bd_light_server_v141\hardware\kit\common\halconfig" -I"C:\git\dev_bd_light_server_v141\protocol\bluetooth\bt_mesh\inc\soc" -I"C:\git\dev_bd_light_server_v141\platform\Device\SiliconLabs\EFR32BG13P\Include" -I"C:\git\dev_bd_light_server_v141\platform\radio\rail_lib\protocol\ieee802154" -I"C:\git\dev_bd_light_server_v141\platform\emlib\inc" -I"C:\git\dev_bd_light_server_v141\protocol\bluetooth\bt_mesh\src" -I"C:\git\dev_bd_light_server_v141\platform\emdrv\sleep\inc" -I"C:\git\dev_bd_light_server_v141\platform\CMSIS\Include" -I"C:\git\dev_bd_light_server_v141\platform\emdrv\gpiointerrupt\inc" -I"C:\git\dev_bd_light_server_v141\platform\emlib\src" -I"C:\git\dev_bd_light_server_v141\hardware\kit\common\bsp" -I"C:\git\dev_bd_light_server_v141\hardware\kit\EFR32BG13_BRD4104A\config" -I"C:\git\dev_bd_light_server_v141\protocol\bluetooth\bt_mesh\inc" -I"C:\git\dev_bd_light_server_v141\platform\bootloader\api" -I"C:\git\dev_bd_light_server_v141\platform\radio\rail_lib\protocol\ble" -I"C:\git\dev_bd_light_server_v141\platform\halconfig\inc\hal-config" -I"C:\git\dev_bd_light_server_v141\platform\radio\rail_lib\chip\efr32\efr32xg1x" -I"C:\git\dev_bd_light_server_v141\platform\emdrv\uartdrv\inc" -I"C:\git\dev_bd_light_server_v141\platform\radio\rail_lib\common" -I"C:\git\dev_bd_light_server_v141\platform\Device\SiliconLabs\EFR32BG13P\Source\GCC" -I"C:\git\dev_bd_light_server_v141\platform\emdrv\sleep\src" -I"C:\git\dev_bd_light_server_v141\hardware\kit\common\drivers" -I"C:\git\dev_bd_light_server_v141\platform\emdrv\common\inc" -I"C:\git\dev_bd_light_server_v141\platform\Device\SiliconLabs\EFR32BG13P\Source" -Os -fno-builtin -flto -Wall -c -fmessage-length=0 -ffunction-sections -fdata-sections -mfpu=fpv4-sp-d16 -mfloat-abi=softfp -MMD -MP -MF"log_module/SEGGER_RTT.d" -MT"log_module/SEGGER_RTT.o" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

log_module/SEGGER_RTT_printf.o: ../log_module/SEGGER_RTT_printf.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM C Compiler'
	arm-none-eabi-gcc -g -gdwarf-2 -mcpu=cortex-m4 -mthumb -std=c99 '-D__HEAP_SIZE=0x1200' '-D__STACK_SIZE=0x1000' '-DHAL_CONFIG=1' '-DMESH_LIB_NATIVE=1' '-DEFR32BG13P632F512GM48=1' -I"C:\git\dev_bd_light_server_v141" -I"C:\git\dev_bd_light_server_v141\log_module" -I"C:\git\dev_bd_light_server_v141\protocol\bluetooth\bt_mesh\inc\common" -I"C:\git\dev_bd_light_server_v141\hardware\kit\common\halconfig" -I"C:\git\dev_bd_light_server_v141\protocol\bluetooth\bt_mesh\inc\soc" -I"C:\git\dev_bd_light_server_v141\platform\Device\SiliconLabs\EFR32BG13P\Include" -I"C:\git\dev_bd_light_server_v141\platform\radio\rail_lib\protocol\ieee802154" -I"C:\git\dev_bd_light_server_v141\platform\emlib\inc" -I"C:\git\dev_bd_light_server_v141\protocol\bluetooth\bt_mesh\src" -I"C:\git\dev_bd_light_server_v141\platform\emdrv\sleep\inc" -I"C:\git\dev_bd_light_server_v141\platform\CMSIS\Include" -I"C:\git\dev_bd_light_server_v141\platform\emdrv\gpiointerrupt\inc" -I"C:\git\dev_bd_light_server_v141\platform\emlib\src" -I"C:\git\dev_bd_light_server_v141\hardware\kit\common\bsp" -I"C:\git\dev_bd_light_server_v141\hardware\kit\EFR32BG13_BRD4104A\config" -I"C:\git\dev_bd_light_server_v141\protocol\bluetooth\bt_mesh\inc" -I"C:\git\dev_bd_light_server_v141\platform\bootloader\api" -I"C:\git\dev_bd_light_server_v141\platform\radio\rail_lib\protocol\ble" -I"C:\git\dev_bd_light_server_v141\platform\halconfig\inc\hal-config" -I"C:\git\dev_bd_light_server_v141\platform\radio\rail_lib\chip\efr32\efr32xg1x" -I"C:\git\dev_bd_light_server_v141\platform\emdrv\uartdrv\inc" -I"C:\git\dev_bd_light_server_v141\platform\radio\rail_lib\common" -I"C:\git\dev_bd_light_server_v141\platform\Device\SiliconLabs\EFR32BG13P\Source\GCC" -I"C:\git\dev_bd_light_server_v141\platform\emdrv\sleep\src" -I"C:\git\dev_bd_light_server_v141\hardware\kit\common\drivers" -I"C:\git\dev_bd_light_server_v141\platform\emdrv\common\inc" -I"C:\git\dev_bd_light_server_v141\platform\Device\SiliconLabs\EFR32BG13P\Source" -Os -fno-builtin -flto -Wall -c -fmessage-length=0 -ffunction-sections -fdata-sections -mfpu=fpv4-sp-d16 -mfloat-abi=softfp -MMD -MP -MF"log_module/SEGGER_RTT_printf.d" -MT"log_module/SEGGER_RTT_printf.o" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

log_module/log.o: ../log_module/log.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM C Compiler'
	arm-none-eabi-gcc -g -gdwarf-2 -mcpu=cortex-m4 -mthumb -std=c99 '-D__HEAP_SIZE=0x1200' '-D__STACK_SIZE=0x1000' '-DHAL_CONFIG=1' '-DMESH_LIB_NATIVE=1' '-DEFR32BG13P632F512GM48=1' -I"C:\git\dev_bd_light_server_v141" -I"C:\git\dev_bd_light_server_v141\log_module" -I"C:\git\dev_bd_light_server_v141\protocol\bluetooth\bt_mesh\inc\common" -I"C:\git\dev_bd_light_server_v141\hardware\kit\common\halconfig" -I"C:\git\dev_bd_light_server_v141\protocol\bluetooth\bt_mesh\inc\soc" -I"C:\git\dev_bd_light_server_v141\platform\Device\SiliconLabs\EFR32BG13P\Include" -I"C:\git\dev_bd_light_server_v141\platform\radio\rail_lib\protocol\ieee802154" -I"C:\git\dev_bd_light_server_v141\platform\emlib\inc" -I"C:\git\dev_bd_light_server_v141\protocol\bluetooth\bt_mesh\src" -I"C:\git\dev_bd_light_server_v141\platform\emdrv\sleep\inc" -I"C:\git\dev_bd_light_server_v141\platform\CMSIS\Include" -I"C:\git\dev_bd_light_server_v141\platform\emdrv\gpiointerrupt\inc" -I"C:\git\dev_bd_light_server_v141\platform\emlib\src" -I"C:\git\dev_bd_light_server_v141\hardware\kit\common\bsp" -I"C:\git\dev_bd_light_server_v141\hardware\kit\EFR32BG13_BRD4104A\config" -I"C:\git\dev_bd_light_server_v141\protocol\bluetooth\bt_mesh\inc" -I"C:\git\dev_bd_light_server_v141\platform\bootloader\api" -I"C:\git\dev_bd_light_server_v141\platform\radio\rail_lib\protocol\ble" -I"C:\git\dev_bd_light_server_v141\platform\halconfig\inc\hal-config" -I"C:\git\dev_bd_light_server_v141\platform\radio\rail_lib\chip\efr32\efr32xg1x" -I"C:\git\dev_bd_light_server_v141\platform\emdrv\uartdrv\inc" -I"C:\git\dev_bd_light_server_v141\platform\radio\rail_lib\common" -I"C:\git\dev_bd_light_server_v141\platform\Device\SiliconLabs\EFR32BG13P\Source\GCC" -I"C:\git\dev_bd_light_server_v141\platform\emdrv\sleep\src" -I"C:\git\dev_bd_light_server_v141\hardware\kit\common\drivers" -I"C:\git\dev_bd_light_server_v141\platform\emdrv\common\inc" -I"C:\git\dev_bd_light_server_v141\platform\Device\SiliconLabs\EFR32BG13P\Source" -Os -fno-builtin -flto -Wall -c -fmessage-length=0 -ffunction-sections -fdata-sections -mfpu=fpv4-sp-d16 -mfloat-abi=softfp -MMD -MP -MF"log_module/log.d" -MT"log_module/log.o" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

log_module/retargetio.o: ../log_module/retargetio.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM C Compiler'
	arm-none-eabi-gcc -g -gdwarf-2 -mcpu=cortex-m4 -mthumb -std=c99 '-D__HEAP_SIZE=0x1200' '-D__STACK_SIZE=0x1000' '-DHAL_CONFIG=1' '-DMESH_LIB_NATIVE=1' '-DEFR32BG13P632F512GM48=1' -I"C:\git\dev_bd_light_server_v141" -I"C:\git\dev_bd_light_server_v141\log_module" -I"C:\git\dev_bd_light_server_v141\protocol\bluetooth\bt_mesh\inc\common" -I"C:\git\dev_bd_light_server_v141\hardware\kit\common\halconfig" -I"C:\git\dev_bd_light_server_v141\protocol\bluetooth\bt_mesh\inc\soc" -I"C:\git\dev_bd_light_server_v141\platform\Device\SiliconLabs\EFR32BG13P\Include" -I"C:\git\dev_bd_light_server_v141\platform\radio\rail_lib\protocol\ieee802154" -I"C:\git\dev_bd_light_server_v141\platform\emlib\inc" -I"C:\git\dev_bd_light_server_v141\protocol\bluetooth\bt_mesh\src" -I"C:\git\dev_bd_light_server_v141\platform\emdrv\sleep\inc" -I"C:\git\dev_bd_light_server_v141\platform\CMSIS\Include" -I"C:\git\dev_bd_light_server_v141\platform\emdrv\gpiointerrupt\inc" -I"C:\git\dev_bd_light_server_v141\platform\emlib\src" -I"C:\git\dev_bd_light_server_v141\hardware\kit\common\bsp" -I"C:\git\dev_bd_light_server_v141\hardware\kit\EFR32BG13_BRD4104A\config" -I"C:\git\dev_bd_light_server_v141\protocol\bluetooth\bt_mesh\inc" -I"C:\git\dev_bd_light_server_v141\platform\bootloader\api" -I"C:\git\dev_bd_light_server_v141\platform\radio\rail_lib\protocol\ble" -I"C:\git\dev_bd_light_server_v141\platform\halconfig\inc\hal-config" -I"C:\git\dev_bd_light_server_v141\platform\radio\rail_lib\chip\efr32\efr32xg1x" -I"C:\git\dev_bd_light_server_v141\platform\emdrv\uartdrv\inc" -I"C:\git\dev_bd_light_server_v141\platform\radio\rail_lib\common" -I"C:\git\dev_bd_light_server_v141\platform\Device\SiliconLabs\EFR32BG13P\Source\GCC" -I"C:\git\dev_bd_light_server_v141\platform\emdrv\sleep\src" -I"C:\git\dev_bd_light_server_v141\hardware\kit\common\drivers" -I"C:\git\dev_bd_light_server_v141\platform\emdrv\common\inc" -I"C:\git\dev_bd_light_server_v141\platform\Device\SiliconLabs\EFR32BG13P\Source" -Os -fno-builtin -flto -Wall -c -fmessage-length=0 -ffunction-sections -fdata-sections -mfpu=fpv4-sp-d16 -mfloat-abi=softfp -MMD -MP -MF"log_module/retargetio.d" -MT"log_module/retargetio.o" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

log_module/retargetserial.o: ../log_module/retargetserial.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM C Compiler'
	arm-none-eabi-gcc -g -gdwarf-2 -mcpu=cortex-m4 -mthumb -std=c99 '-D__HEAP_SIZE=0x1200' '-D__STACK_SIZE=0x1000' '-DHAL_CONFIG=1' '-DMESH_LIB_NATIVE=1' '-DEFR32BG13P632F512GM48=1' -I"C:\git\dev_bd_light_server_v141" -I"C:\git\dev_bd_light_server_v141\log_module" -I"C:\git\dev_bd_light_server_v141\protocol\bluetooth\bt_mesh\inc\common" -I"C:\git\dev_bd_light_server_v141\hardware\kit\common\halconfig" -I"C:\git\dev_bd_light_server_v141\protocol\bluetooth\bt_mesh\inc\soc" -I"C:\git\dev_bd_light_server_v141\platform\Device\SiliconLabs\EFR32BG13P\Include" -I"C:\git\dev_bd_light_server_v141\platform\radio\rail_lib\protocol\ieee802154" -I"C:\git\dev_bd_light_server_v141\platform\emlib\inc" -I"C:\git\dev_bd_light_server_v141\protocol\bluetooth\bt_mesh\src" -I"C:\git\dev_bd_light_server_v141\platform\emdrv\sleep\inc" -I"C:\git\dev_bd_light_server_v141\platform\CMSIS\Include" -I"C:\git\dev_bd_light_server_v141\platform\emdrv\gpiointerrupt\inc" -I"C:\git\dev_bd_light_server_v141\platform\emlib\src" -I"C:\git\dev_bd_light_server_v141\hardware\kit\common\bsp" -I"C:\git\dev_bd_light_server_v141\hardware\kit\EFR32BG13_BRD4104A\config" -I"C:\git\dev_bd_light_server_v141\protocol\bluetooth\bt_mesh\inc" -I"C:\git\dev_bd_light_server_v141\platform\bootloader\api" -I"C:\git\dev_bd_light_server_v141\platform\radio\rail_lib\protocol\ble" -I"C:\git\dev_bd_light_server_v141\platform\halconfig\inc\hal-config" -I"C:\git\dev_bd_light_server_v141\platform\radio\rail_lib\chip\efr32\efr32xg1x" -I"C:\git\dev_bd_light_server_v141\platform\emdrv\uartdrv\inc" -I"C:\git\dev_bd_light_server_v141\platform\radio\rail_lib\common" -I"C:\git\dev_bd_light_server_v141\platform\Device\SiliconLabs\EFR32BG13P\Source\GCC" -I"C:\git\dev_bd_light_server_v141\platform\emdrv\sleep\src" -I"C:\git\dev_bd_light_server_v141\hardware\kit\common\drivers" -I"C:\git\dev_bd_light_server_v141\platform\emdrv\common\inc" -I"C:\git\dev_bd_light_server_v141\platform\Device\SiliconLabs\EFR32BG13P\Source" -Os -fno-builtin -flto -Wall -c -fmessage-length=0 -ffunction-sections -fdata-sections -mfpu=fpv4-sp-d16 -mfloat-abi=softfp -MMD -MP -MF"log_module/retargetserial.d" -MT"log_module/retargetserial.o" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


