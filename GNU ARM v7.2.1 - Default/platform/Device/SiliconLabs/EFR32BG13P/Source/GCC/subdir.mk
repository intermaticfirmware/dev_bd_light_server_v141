################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../platform/Device/SiliconLabs/EFR32BG13P/Source/GCC/startup_efr32bg13p.c 

OBJS += \
./platform/Device/SiliconLabs/EFR32BG13P/Source/GCC/startup_efr32bg13p.o 

C_DEPS += \
./platform/Device/SiliconLabs/EFR32BG13P/Source/GCC/startup_efr32bg13p.d 


# Each subdirectory must supply rules for building sources it contributes
platform/Device/SiliconLabs/EFR32BG13P/Source/GCC/startup_efr32bg13p.o: ../platform/Device/SiliconLabs/EFR32BG13P/Source/GCC/startup_efr32bg13p.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM C Compiler'
	arm-none-eabi-gcc -g -gdwarf-2 -mcpu=cortex-m4 -mthumb -std=c99 '-D__HEAP_SIZE=0x1200' '-D__STACK_SIZE=0x1000' '-DHAL_CONFIG=1' '-DMESH_LIB_NATIVE=1' '-DEFR32BG13P632F512GM48=1' -I"C:\git\dev_bd_light_server_v141" -I"C:\git\dev_bd_light_server_v141\log_module" -I"C:\git\dev_bd_light_server_v141\protocol\bluetooth\bt_mesh\inc\common" -I"C:\git\dev_bd_light_server_v141\hardware\kit\common\halconfig" -I"C:\git\dev_bd_light_server_v141\protocol\bluetooth\bt_mesh\inc\soc" -I"C:\git\dev_bd_light_server_v141\platform\Device\SiliconLabs\EFR32BG13P\Include" -I"C:\git\dev_bd_light_server_v141\platform\radio\rail_lib\protocol\ieee802154" -I"C:\git\dev_bd_light_server_v141\platform\emlib\inc" -I"C:\git\dev_bd_light_server_v141\protocol\bluetooth\bt_mesh\src" -I"C:\git\dev_bd_light_server_v141\platform\emdrv\sleep\inc" -I"C:\git\dev_bd_light_server_v141\platform\CMSIS\Include" -I"C:\git\dev_bd_light_server_v141\platform\emdrv\gpiointerrupt\inc" -I"C:\git\dev_bd_light_server_v141\platform\emlib\src" -I"C:\git\dev_bd_light_server_v141\hardware\kit\common\bsp" -I"C:\git\dev_bd_light_server_v141\hardware\kit\EFR32BG13_BRD4104A\config" -I"C:\git\dev_bd_light_server_v141\protocol\bluetooth\bt_mesh\inc" -I"C:\git\dev_bd_light_server_v141\platform\bootloader\api" -I"C:\git\dev_bd_light_server_v141\platform\radio\rail_lib\protocol\ble" -I"C:\git\dev_bd_light_server_v141\platform\halconfig\inc\hal-config" -I"C:\git\dev_bd_light_server_v141\platform\radio\rail_lib\chip\efr32\efr32xg1x" -I"C:\git\dev_bd_light_server_v141\platform\emdrv\uartdrv\inc" -I"C:\git\dev_bd_light_server_v141\platform\radio\rail_lib\common" -I"C:\git\dev_bd_light_server_v141\platform\Device\SiliconLabs\EFR32BG13P\Source\GCC" -I"C:\git\dev_bd_light_server_v141\platform\emdrv\sleep\src" -I"C:\git\dev_bd_light_server_v141\hardware\kit\common\drivers" -I"C:\git\dev_bd_light_server_v141\platform\emdrv\common\inc" -I"C:\git\dev_bd_light_server_v141\platform\Device\SiliconLabs\EFR32BG13P\Source" -Os -fno-builtin -flto -Wall -c -fmessage-length=0 -ffunction-sections -fdata-sections -mfpu=fpv4-sp-d16 -mfloat-abi=softfp -MMD -MP -MF"platform/Device/SiliconLabs/EFR32BG13P/Source/GCC/startup_efr32bg13p.d" -MT"platform/Device/SiliconLabs/EFR32BG13P/Source/GCC/startup_efr32bg13p.o" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


