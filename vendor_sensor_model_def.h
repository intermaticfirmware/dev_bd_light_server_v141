//-----------------------------------------------------------------------------
//   All Rights Reserved
//  Intermatic Incorporated
//  Copyright 2019
//-----------------------------------------------------------------------------
//
//  Description: Contains constants, macros, structure
//  definitions, and public prototypes owned by
//
//-----------------------------------------------------------------------------
#ifndef _VENDOR_SENSOR_MODEL_DEF_H_
#define _VENDOR_SENSOR_MODEL_DEF_H_

#define PRIMARY_ELEMENT								0

#define SILABS_VENDOR_ID							0x02FF

#define VENDOR_SENSOR_SERVER_ID						0x1111 //0x1100
#define VENDOR_SENSOR_SETUP_SERVER_ID				0x1101
#define VENDOR_SENSOR_CLIENT_ID                     0x1112 //0x2222 //0x1102

#define VENDOR_SENSOR_OCC_GRP_ADDR                  0xC00A

#define VENDOR_SENSOR_SERVER_LIGHT_ID               0x1113
#define VENDOR_SENSOR_CLIENT_LIGHT_ID               0x1114

#define VENDOR_SENSOR_LIGHT_GRP_ADDR                0xC00C

#define TEMP_DATA_LENGTH							4

#define UPDATE_INTERVAL_LENGTH						1
#define UNIT_DATA_LENGTH							1

#define OCC_DATA_LENGTH                             1
#define SENSOR_SETTING_DATA_LENGTH                  1

#define NUMBER_OF_OPCODES							4
#define LIGHT_VEND_NUMBER_OF_OPCODES                8

#define ACK_REQ										(0x1)
#define STATUS_UPDATE_REQ							(0x2)

#define INDEX_OF(x)									((x) - 1)

#define MOTION_SENSED_PROPERTY_ID                   0x0042

#define PRESENCE_DETECTED_PROPERTY_ID               0x004D

#define AMBIENT_LIGHT_PROPERTY_ID                   0x004E

#define BIT7                                        0x80

// vendor sensor client opcodes
typedef enum {
	sensor_get = 0x01, 
	sensor_settings_set = 0x02
} vendor_client_occ_sensor_t;

// vendor sensor server opcodes
typedef enum {
	sensor_status = 0x03, //0x52,
	sensor_setting_status = 0x04 //0x5B
}vendor_server_occ_sensor_t;

typedef enum {
	sensor_cadence_get = 0x05,
	sensor_cadence_set = 0x06,
	sensor_cadence_set_unack = 0x07
}vendor_client_light_sensor_t;

typedef enum {
	sensor_cadence_status = 0x08
}vendor_server_light_sensor_t;

typedef struct {
	uint16_t elem_index;
	uint16_t vendor_id;
	uint16_t model_id;
	uint8_t publish; // publish - 1, not - 0
	uint8_t opcodes_len;
	uint8_t opcodes_data[NUMBER_OF_OPCODES];
} vendor_model_t;

typedef struct {
	uint16_t elem_index;
	uint16_t vendor_id;
	uint16_t model_id;
	uint8_t publish; // publish - 1, not - 0
	uint8_t opcodes_len;
	uint8_t opcodes_data[LIGHT_VEND_NUMBER_OF_OPCODES];
} light_vendor_model_t;

// enums for settings

typedef enum {
	PIR_SENSITIVITY_ID = 0,
	HF_SENSITIVITY_ID,
	TRIGGER_LOGIC_ID,
	OCC_MODE_ID,
	OCC_TIME_DELAY_ID,
	WALK_THRU_ID
}sensor_setting_property_id_t;

typedef enum {
	THRESHOLD_MSG_ID = 0,
	THRESHOLD_SLEEP_ID = 1
}light_sensor_setting_property_id_t;

typedef enum {
    PIR_SENSITIVITY_25 = 0,
    PIR_SENSITIVITY_50 = 1,
    PIR_SENSITIVITY_75 = 2,
    PIR_SENSITIVITY_100 = 3
}pir_sensitivity_t;

typedef enum {
    HF_SENSITIVITY_25 = 0,
    HF_SENSITIVITY_50 = 1,
    HF_SENSITIVITY_75 = 2,
    HF_SENSITIVITY_100 = 3
}hf_sensitivity_t;

typedef enum {
    TRIGGER_LOGIC_BOTH = 0,
    TRIGGER_LOGIC_PIR = 1,
    TRIGGER_LOGIC_HF = 2,
	TRIGGER_LOGIC_PIR_HF = 3
}trigger_logic_t;

typedef enum {
	OCCUPANCY_MODE = 0,
	VACANCY_MODE = 1
}occ_vac_mode_t;

typedef enum {
	WALK_THRU_DIS = 0,
	WALK_THRU_EN  = 1
}walk_thru_t;

// enum for host messages

typedef enum {
	OCC_DETECTED = 0x01,
	VAC_DETECTED = 0x02
}occ_vac_trigger_t;

typedef struct {
	uint16_t  sensor_property_id;
	uint16_t  sensor_setting_property_id;
	uint8_t   setting;
} __attribute__((packed)) sensor_settings_t;

typedef struct {
	uint16_t  sensor_property_id;
	uint16_t  sensor_setting_property_id;
	uint8_t   setting_array[3];
} __attribute__((packed)) sensor_settings_arr_t;

enum {
	MPID_HEADER_A = 0,
	MPID_HEADER_B = 1
};

typedef struct {
	uint16_t fast_cadence_high;
	uint16_t fast_cadence_low;
	uint16_t status_min_int :8;
	uint16_t trigger_delta_up;
	uint16_t trigger_delta_down;
	uint16_t status_trig_type : 1;
	uint16_t fast_cadence_per_div : 7;
	uint16_t property_id;
}__attribute__((packed))cadence_setting_msg_t;


typedef struct {
	uint16_t property_id : 11;
	uint16_t length : 4;
	uint16_t format : 1;
}__attribute__((packed))format_A_MPID_t; // note bitfield order will depend on complier

typedef struct {
	uint16_t length : 7;
	uint16_t format : 1;
	uint16_t property_id;
}__attribute__((packed))format_B_MPID_t; // note bitfield order will depend on complier

typedef struct {
	format_B_MPID_t header;
	uint8_t sensor_data;
}__attribute__((packed))sensor_msg_t;

typedef struct {
	format_B_MPID_t header;
	uint8_t sensor_data[3];
}__attribute__((packed))light_sensor_msg_t;

#endif
